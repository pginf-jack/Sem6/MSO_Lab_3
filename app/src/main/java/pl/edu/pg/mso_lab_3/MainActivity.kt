package pl.edu.pg.mso_lab_3

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.text.style.TabStopSpan.Standard
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.io.*
import java.nio.charset.StandardCharsets
import java.util.stream.Collectors


class MainActivity : AppCompatActivity() {
    val textBox: EditText by lazy { findViewById(R.id.textbox) }
    val lastOpenedText: TextView by lazy { findViewById(R.id.lastOpenedText) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val prefs = getSharedPreferences("remembered_stuff", Context.MODE_PRIVATE)
        val lastOpened = prefs.getString("lastOpened", "")
        lastOpenedText.text = lastOpened
    }

    fun openFile(view: View) {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "*/*"
        if (lastOpenedText.text.toString() != "") {
            intent.putExtra(
                DocumentsContract.EXTRA_INITIAL_URI,
                Uri.parse(lastOpenedText.text.toString())
            )
        } else {
            intent.putExtra(
                DocumentsContract.EXTRA_INITIAL_URI,
                Environment.DIRECTORY_DOCUMENTS
            )
        }
        startActivityForResult(intent, 111)
    }

    fun saveFile(view: View) {
        val intent = Intent(Intent.ACTION_CREATE_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "application/txt"
        intent.putExtra(Intent.EXTRA_TITLE, "untitled.txt")
        intent.putExtra(
            DocumentsContract.EXTRA_INITIAL_URI,
            Environment.DIRECTORY_DOCUMENTS
        )
        startActivityForResult(intent, 222)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?) {
        super.onActivityResult(requestCode, resultCode, resultData)
        if (requestCode == 111 && resultCode == RESULT_OK) {
            var uri: Uri? = null
            if (resultData != null) {
                uri = resultData.data
                val inputStream: InputStream? = contentResolver.openInputStream(uri!!)
                val text = BufferedReader(
                    InputStreamReader(inputStream!!)).lines().collect(Collectors.joining("\n")
                )
                textBox.text.clear()
                textBox.text.append(text)
                val prefs = getSharedPreferences("remembered_stuff", Context.MODE_PRIVATE)
                val prefsEditor = prefs.edit()
                prefsEditor.putString("lastOpened", uri.toString())
                prefsEditor.commit()
                lastOpenedText.text = uri.toString()
            }
        } else if (requestCode == 222 && resultCode == RESULT_OK) {
            var uri: Uri? = null
            if (resultData != null) {
                uri = resultData.data
                val pfd = contentResolver.openFileDescriptor(uri!!, "w")
                val fileOutputStream = FileOutputStream(pfd!!.fileDescriptor)
                val writer = PrintWriter(fileOutputStream)
                writer.print(textBox.text)
                writer.close()
                fileOutputStream.close()
                pfd!!.close()
                val prefs = getSharedPreferences("remembered_stuff", Context.MODE_PRIVATE)
                val prefsEditor = prefs.edit()
                prefsEditor.putString("lastOpened", uri.toString())
                prefsEditor.commit()
                lastOpenedText.text = uri.toString()
            }
        }
    }
}